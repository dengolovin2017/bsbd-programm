﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class Journal : Form
    {
        private static Journal f;

        private int idCurrent = -1;

        private Journal()
        {
            InitializeComponent();
        }

        public static Journal GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new Journal();
            }

            return f;
        }

        private string GetSelectedJournal()
        {
            return journalDataGridView.Columns[journalDataGridView.CurrentCell.ColumnIndex].DataPropertyName;
        }

        private void journalBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
            this.Validate();
            this.journalBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Journal_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Journal". При необходимости она может быть перемещена или удалена.
            this.journalTableAdapter.Fill(this.online_UniversityDataSet.Journal);

        }

        private void ButtonFind_Click(object sender, EventArgs e)
        {
            if (textBoxF.Text == "")
            {
                MessageBox.Show("Поле пустое!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int index;

            try
            {
                index = journalBindingSource.Find(GetSelectedJournal(), textBoxF.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка поиска \n" + ex.Message);
                return;
            }

            if (index > -1)
            {
                journalBindingSource.Position = index;
            }
            else
            {
                MessageBox.Show("Нет такого!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                journalBindingSource.Position = 0;
            }
        }

        public int ShowSelectForm(int id)
        {
            idCurrent = id;
            if (ShowDialog() == DialogResult.OK)
            {
                return (int)((DataRowView)journalBindingSource.Current)["student_id"];

            }
            else
            {
                return -1;
            }
        }

        private void checkBoxFind_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFind.Checked)
            {
                if (textBoxF.Text == "")
                {
                    MessageBox.Show("Поле пустое!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    try
                    {
                        journalBindingSource.Filter = GetSelectedJournal() + "='" + textBoxF.Text + "'";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ошибка фильтрации \n" + ex.Message);
                    }
                }
            }
            else
            {
                journalBindingSource.Filter = "";
            }

            if (journalBindingSource.Count == 0)
            {
                MessageBox.Show("Нет такого");
                journalBindingSource.Filter = "";
                checkBoxFind.Checked = false;
            }
        }

        private void Journal_Shown(object sender, EventArgs e)
        {
            journalBindingSource.Position = journalBindingSource.Find("id", idCurrent);
        }
    }
}
