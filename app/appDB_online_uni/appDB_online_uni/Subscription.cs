﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class Subscription : Form
    {
        private static Subscription f;

        private Subscription()
        {
            InitializeComponent();
        }

        public static Subscription GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new Subscription();
            }

            return f;
        }

        private string GetSelectedstudent_Course()
        {
            return student_CourseDataGridView.Columns[student_CourseDataGridView.CurrentCell.ColumnIndex].DataPropertyName;
        }

        private void student_CourseBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
            this.Validate();
            this.student_CourseBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Subscription_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Student_Course". При необходимости она может быть перемещена или удалена.
            this.student_CourseTableAdapter.Fill(this.online_UniversityDataSet.Student_Course);

        }

        private void ButtonFind_Click(object sender, EventArgs e)
        {
            if (textBoxF.Text == "")
            {
                MessageBox.Show("Поле пустое!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int index;

            try
            {
                index = student_CourseBindingSource.Find(GetSelectedstudent_Course(), textBoxF.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка поиска \n" + ex.Message);
                return;
            }

            if (index > -1)
            {
                student_CourseBindingSource.Position = index;
            }
            else
            {
                MessageBox.Show("Нет такого!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                student_CourseBindingSource.Position = 0;
            }
        }

        private void checkBoxFind_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFind.Checked)
            {
                if (textBoxF.Text == "")
                {
                    MessageBox.Show("Поле пустое!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    try
                    {
                        student_CourseBindingSource.Filter = GetSelectedstudent_Course() + "='" + textBoxF.Text + "'";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ошибка фильтрации \n" + ex.Message);
                    }
                }
            }
            else
            {
                student_CourseBindingSource.Filter = "";
            }

            if (student_CourseBindingSource.Count == 0)
            {
                MessageBox.Show("Нет такого");
                student_CourseBindingSource.Filter = "";
                checkBoxFind.Checked = false;
            }
        }
    }
}
