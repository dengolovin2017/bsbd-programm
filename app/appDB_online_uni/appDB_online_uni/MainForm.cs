﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace appDB_online_uni
{
    public partial class MainForm : Form
    {
        private string role;
        /*
        private SqlConnection sqlConnection;
        
        private SqlDataAdapter adapter = null;

        private System.Data.DataTable table = null;
        */

        public MainForm(string role)
        {
            InitializeComponent();
            this.role = role;

            //dataGridView1.BackgroundColor = BackColor;
        }

       

        private void CloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = MessageBox.Show("Вы это серьезно??", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes;
            Application.Exit();
        }

        private void AboutProgrammToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This programm needs a true hero (C) TUSUR, KIBEVS, Golovin Denis Borisovich, 728-1, 2021.", "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            label2.Text = role;
            if (role != "admin")
            {
                adminToolStripMenuItem.Visible = false;
                sQLToolStripMenuItem.Visible = false;
                StudentsToolStripMenuItem.Visible = false;
                TeachersToolStripMenuItem.Visible = false;
                PassportDataToolStripMenuItem.Visible = false;
                SubscriptionToolStripMenuItem.Visible = false;
            }

            if (role == "simple")
            {
                DZToolStripMenuItem.Visible = false;
                JournalToolStripMenuItem.Visible = false;
            }

        }

        private void TeachersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TeacherForm f = TeacherForm.GetInstance();
            f.Show();
        }

        private void PassportDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PassportForm f = PassportForm.GetInstance();
            f.Show();
        }

        private void StudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StudentsForm f = StudentsForm.GetInstance();
            f.Show();
        }

        private void CoursesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Courses f = Courses.GetInstance();
            f.Show();
        }

        private void DZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Homework f = Homework.GetInstance();
            f.Show();
        }

        private void JournalToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            Journal f = Journal.GetInstance();
            f.Show();
        }

        private void SubscriptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Subscription f = Subscription.GetInstance();
            f.Show();          
        }

        private void sqlQueryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SQL_Form f = SQL_Form.GetInstance();
            f.Show();
        }

        private void userBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.userBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);

        }

        private void adminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminWin f = AdminWin.GetInstance();
            f.Show();
        }
    }
}
