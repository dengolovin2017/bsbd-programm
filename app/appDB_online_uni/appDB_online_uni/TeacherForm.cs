﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class TeacherForm : Form
    {
        private static TeacherForm f;

        private TeacherForm()
        {
            InitializeComponent();
        }

        public static TeacherForm GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new TeacherForm();
            }

            return f;
        }

        private void teacherBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
            this.teacherBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TeacherForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Journal". При необходимости она может быть перемещена или удалена.
            this.journalTableAdapter.Fill(this.online_UniversityDataSet.Journal);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Course". При необходимости она может быть перемещена или удалена.
            this.courseTableAdapter.Fill(this.online_UniversityDataSet.Course);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Teacher". При необходимости она может быть перемещена или удалена.
            this.teacherTableAdapter.Fill(this.online_UniversityDataSet.Teacher);

        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            try
            { 
            this.Validate();
            this.courseBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            try
            { 
            this.Validate();
            this.journalBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
