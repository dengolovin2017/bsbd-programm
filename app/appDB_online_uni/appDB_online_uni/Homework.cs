﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class Homework : Form
    {
        private static Homework f;

        private Homework()
        {
            InitializeComponent();
        }

        public static Homework GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new Homework();
            }

            return f;
        }

        private void homeworkBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
            this.Validate();
            this.homeworkBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetSelectedHomework()
        {
            return homeworkDataGridView.Columns[homeworkDataGridView.CurrentCell.ColumnIndex].DataPropertyName;
        }

        private void Homework_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Journal". При необходимости она может быть перемещена или удалена.
            this.journalTableAdapter.Fill(this.online_UniversityDataSet.Journal);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Homework". При необходимости она может быть перемещена или удалена.
            this.homeworkTableAdapter.Fill(this.online_UniversityDataSet.Homework);

        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            try
            { 
            this.Validate();
            this.journalBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            } catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ButtonFind_Click(object sender, EventArgs e)
        {
            if (textBoxF.Text == "")
            {
                MessageBox.Show("Поле пустое!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int index;

            try
            {
                index = homeworkBindingSource.Find(GetSelectedHomework(), textBoxF.Text);    
            }
            catch (Exception ex) 
            {
                MessageBox.Show("Ошибка поиска \n" + ex.Message);
                return;
            }

            if (index > -1)
            {
                homeworkBindingSource.Position = index;
            } else
            {
                MessageBox.Show("Нет такого!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                homeworkBindingSource.Position = 0;
            }
        }

        private void checkBoxFind_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFind.Checked)
            {
                if (textBoxF.Text == "")
                {
                    MessageBox.Show("Поле пустое!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } else
                {
                    try
                    {
                        homeworkBindingSource.Filter = GetSelectedHomework() + "='" + textBoxF.Text + "'";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ошибка фильтрации \n" + ex.Message);
                    }
                }
            } else
            {
                homeworkBindingSource.Filter = "";
            }

            if (homeworkBindingSource.Count == 0)
            {
                MessageBox.Show("Нет такого");
                homeworkBindingSource.Filter = "";
                checkBoxFind.Checked = false;
            }
        }
    }
}
