﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class AdminWin : Form
    {
        private static AdminWin f;

        private AdminWin()
        {
            InitializeComponent();
        }

        public static AdminWin GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new AdminWin();
            }

            return f;
        }

        private void usersBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.usersBindingSource.EndEdit();
            this.tableAdapterManager1.UpdateAll(this.online_UniversityDataSet1);

        }

        private void AdminWin_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet1.Users". При необходимости она может быть перемещена или удалена.
            this.usersTableAdapter.Fill(this.online_UniversityDataSet1.Users);
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.usersDataGridView.CurrentCell.ColumnIndex == 3)
            {
                this.usersDataGridView.CurrentCell.Value = radioButton1.Text;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (this.usersDataGridView.CurrentCell.ColumnIndex == 3)
            {
                this.usersDataGridView.CurrentCell.Value = radioButton2.Text;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (this.usersDataGridView.CurrentCell.ColumnIndex == 3)
            {
                this.usersDataGridView.CurrentCell.Value = radioButton3.Text;
            }
        }
    }
}
