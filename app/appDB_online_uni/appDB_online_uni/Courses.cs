﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class Courses : Form
    {
        private static Courses f;

        System.Drawing.Printing.PrintPageEventArgs p;

        private Courses()
        {
            InitializeComponent();
        }

        public static Courses GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new Courses();
            }

            return f;
        }

        private void courseBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.courseBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Courses_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Homework". При необходимости она может быть перемещена или удалена.
            this.homeworkTableAdapter.Fill(this.online_UniversityDataSet.Homework);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Student_Course". При необходимости она может быть перемещена или удалена.
            this.student_CourseTableAdapter.Fill(this.online_UniversityDataSet.Student_Course);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Course". При необходимости она может быть перемещена или удалена.
            this.courseTableAdapter.Fill(this.online_UniversityDataSet.Course);

        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.student_CourseBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.homeworkBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            countSub.Text = student_CourseBindingSource.Count.ToString();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {


            if (this.Validate() && (this.courseBindingSource != null))
            {
                try
                {
                    this.courseBindingSource.AddNew();
                    this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        
    }
}
