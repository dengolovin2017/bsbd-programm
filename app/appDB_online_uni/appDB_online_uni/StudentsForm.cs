﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class StudentsForm : Form
    {
        private static StudentsForm f;

        

        private StudentsForm()
        {
            InitializeComponent();
        }

        public static StudentsForm GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new StudentsForm();
            }

            return f;
        }

        private void studentBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.studentBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        

        private void StudentsForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Journal". При необходимости она может быть перемещена или удалена.
            this.journalTableAdapter.Fill(this.online_UniversityDataSet.Journal);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Student_Course". При необходимости она может быть перемещена или удалена.
            this.student_CourseTableAdapter.Fill(this.online_UniversityDataSet.Student_Course);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Student". При необходимости она может быть перемещена или удалена.
            this.studentTableAdapter.Fill(this.online_UniversityDataSet.Student);
            DateTime date;
            online_UniversityDataSet.Student_Course.Columns["registration_date"].DefaultValue = DateTime.Now;
            online_UniversityDataSet.Student_Course.Columns["id"].AutoIncrement = true;
            online_UniversityDataSet.Student_Course.Columns["id"].AutoIncrementSeed = 7;
            online_UniversityDataSet.Student_Course.Columns["id"].AutoIncrementStep = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string fileImage = "";
            openFileDialog1.Title = "Укажите файл для фото";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileImage = openFileDialog1.FileName;
                try
                {
                    fotoPictureBox.Image = new Bitmap(openFileDialog1.FileName);
                }
                catch
                {
                    MessageBox.Show("не то выбрал!");
                    return;
                }
            }
            else fileImage = "";
        }

        private void SubSaveItem_Click(object sender, EventArgs e)
        {
            try
            { 
            this.Validate();
            this.student_CourseBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton13_Click(object sender, EventArgs e)
        {
            try
            { 
            this.Validate();
            this.journalBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            } catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = -1;

            if (((DataRowView)studentBindingSource.Current)["id"].ToString() != "")
            {
                id = (int)((DataRowView)studentBindingSource.Current)["id"];
            }
            Journal j = Journal.GetInstance();
            id = j.ShowSelectForm(id);
            
            if (id >= 0)
            {
                MessageBox.Show(id.ToString());
                ((DataRowView)studentBindingSource.Current)[id] = id;
                studentBindingSource.EndEdit();
            }
        }

        private void student_CourseDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
