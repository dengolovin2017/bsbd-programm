﻿namespace appDB_online_uni
{
    partial class SQL_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlSQL = new System.Windows.Forms.TabControl();
            this.tabPageExample = new System.Windows.Forms.TabPage();
            this.dataGridViewSelect = new System.Windows.Forms.DataGridView();
            this.groupBoxSelect = new System.Windows.Forms.GroupBox();
            this.radioButtonStudentName = new System.Windows.Forms.RadioButton();
            this.radioButtonStudents = new System.Windows.Forms.RadioButton();
            this.radioButtonCourses = new System.Windows.Forms.RadioButton();
            this.tabPageSelect = new System.Windows.Forms.TabPage();
            this.dataGridViewS = new System.Windows.Forms.DataGridView();
            this.groupBoxSelectChooseCourse = new System.Windows.Forms.GroupBox();
            this.buttonFindCourses = new System.Windows.Forms.Button();
            this.textBoxPriceFrom = new System.Windows.Forms.TextBox();
            this.groupBoxDetail = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButtonMarketing = new System.Windows.Forms.RadioButton();
            this.radioButtonDesign = new System.Windows.Forms.RadioButton();
            this.radioButtonIT = new System.Windows.Forms.RadioButton();
            this.labelPriceTo = new System.Windows.Forms.Label();
            this.textBoxCourseName = new System.Windows.Forms.TextBox();
            this.textBoxPriceTo = new System.Windows.Forms.TextBox();
            this.labeCourseName = new System.Windows.Forms.Label();
            this.labelPriceFrom = new System.Windows.Forms.Label();
            this.checkBoxSort = new System.Windows.Forms.CheckBox();
            this.tabPageSubquery = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBoxSubquery = new System.Windows.Forms.GroupBox();
            this.buttonExecute = new System.Windows.Forms.Button();
            this.textBoxNumberSub = new System.Windows.Forms.TextBox();
            this.labelNumber = new System.Windows.Forms.Label();
            this.radioButtonNoCorrelated = new System.Windows.Forms.RadioButton();
            this.radioButtonCorrelated = new System.Windows.Forms.RadioButton();
            this.tabPageDML = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.buttonShow = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBoxNam = new System.Windows.Forms.TextBox();
            this.labC = new System.Windows.Forms.Label();
            this.groupBoxoperat = new System.Windows.Forms.GroupBox();
            this.ExecuteButt = new System.Windows.Forms.Button();
            this.textBoxNumber = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxSeries = new System.Windows.Forms.TextBox();
            this.labelA = new System.Windows.Forms.Label();
            this.radioButtonDel = new System.Windows.Forms.RadioButton();
            this.radioButtonUpt = new System.Windows.Forms.RadioButton();
            this.radioButtonInsert = new System.Windows.Forms.RadioButton();
            this.tabControlSQL.SuspendLayout();
            this.tabPageExample.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSelect)).BeginInit();
            this.groupBoxSelect.SuspendLayout();
            this.tabPageSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewS)).BeginInit();
            this.groupBoxSelectChooseCourse.SuspendLayout();
            this.groupBoxDetail.SuspendLayout();
            this.tabPageSubquery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBoxSubquery.SuspendLayout();
            this.tabPageDML.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel.SuspendLayout();
            this.groupBoxoperat.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlSQL
            // 
            this.tabControlSQL.Controls.Add(this.tabPageExample);
            this.tabControlSQL.Controls.Add(this.tabPageSelect);
            this.tabControlSQL.Controls.Add(this.tabPageSubquery);
            this.tabControlSQL.Controls.Add(this.tabPageDML);
            this.tabControlSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSQL.Location = new System.Drawing.Point(0, 0);
            this.tabControlSQL.Name = "tabControlSQL";
            this.tabControlSQL.SelectedIndex = 0;
            this.tabControlSQL.Size = new System.Drawing.Size(800, 450);
            this.tabControlSQL.TabIndex = 0;
            // 
            // tabPageExample
            // 
            this.tabPageExample.Controls.Add(this.dataGridViewSelect);
            this.tabPageExample.Controls.Add(this.groupBoxSelect);
            this.tabPageExample.Location = new System.Drawing.Point(4, 22);
            this.tabPageExample.Name = "tabPageExample";
            this.tabPageExample.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageExample.Size = new System.Drawing.Size(792, 424);
            this.tabPageExample.TabIndex = 0;
            this.tabPageExample.Text = "Example query";
            this.tabPageExample.UseVisualStyleBackColor = true;
            // 
            // dataGridViewSelect
            // 
            this.dataGridViewSelect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSelect.Location = new System.Drawing.Point(3, 54);
            this.dataGridViewSelect.Name = "dataGridViewSelect";
            this.dataGridViewSelect.Size = new System.Drawing.Size(786, 367);
            this.dataGridViewSelect.TabIndex = 1;
            // 
            // groupBoxSelect
            // 
            this.groupBoxSelect.Controls.Add(this.radioButtonStudentName);
            this.groupBoxSelect.Controls.Add(this.radioButtonStudents);
            this.groupBoxSelect.Controls.Add(this.radioButtonCourses);
            this.groupBoxSelect.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxSelect.Location = new System.Drawing.Point(3, 3);
            this.groupBoxSelect.Name = "groupBoxSelect";
            this.groupBoxSelect.Size = new System.Drawing.Size(786, 51);
            this.groupBoxSelect.TabIndex = 0;
            this.groupBoxSelect.TabStop = false;
            this.groupBoxSelect.Text = "data query";
            // 
            // radioButtonStudentName
            // 
            this.radioButtonStudentName.AutoSize = true;
            this.radioButtonStudentName.Location = new System.Drawing.Point(412, 19);
            this.radioButtonStudentName.Name = "radioButtonStudentName";
            this.radioButtonStudentName.Size = new System.Drawing.Size(113, 17);
            this.radioButtonStudentName.TabIndex = 2;
            this.radioButtonStudentName.TabStop = true;
            this.radioButtonStudentName.Text = "Names of students";
            this.radioButtonStudentName.UseVisualStyleBackColor = true;
            this.radioButtonStudentName.CheckedChanged += new System.EventHandler(this.radioButtonTeachers_CheckedChanged);
            // 
            // radioButtonStudents
            // 
            this.radioButtonStudents.AutoSize = true;
            this.radioButtonStudents.Location = new System.Drawing.Point(306, 19);
            this.radioButtonStudents.Name = "radioButtonStudents";
            this.radioButtonStudents.Size = new System.Drawing.Size(67, 17);
            this.radioButtonStudents.TabIndex = 1;
            this.radioButtonStudents.TabStop = true;
            this.radioButtonStudents.Text = "Students";
            this.radioButtonStudents.UseVisualStyleBackColor = true;
            this.radioButtonStudents.CheckedChanged += new System.EventHandler(this.radioButtonStudents_CheckedChanged);
            // 
            // radioButtonCourses
            // 
            this.radioButtonCourses.AutoSize = true;
            this.radioButtonCourses.Location = new System.Drawing.Point(191, 19);
            this.radioButtonCourses.Name = "radioButtonCourses";
            this.radioButtonCourses.Size = new System.Drawing.Size(63, 17);
            this.radioButtonCourses.TabIndex = 0;
            this.radioButtonCourses.TabStop = true;
            this.radioButtonCourses.Text = "Courses";
            this.radioButtonCourses.UseVisualStyleBackColor = true;
            this.radioButtonCourses.CheckedChanged += new System.EventHandler(this.radioButtonCourses_CheckedChanged);
            // 
            // tabPageSelect
            // 
            this.tabPageSelect.Controls.Add(this.dataGridViewS);
            this.tabPageSelect.Controls.Add(this.groupBoxSelectChooseCourse);
            this.tabPageSelect.Location = new System.Drawing.Point(4, 22);
            this.tabPageSelect.Name = "tabPageSelect";
            this.tabPageSelect.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSelect.Size = new System.Drawing.Size(792, 424);
            this.tabPageSelect.TabIndex = 1;
            this.tabPageSelect.Text = "SELECT";
            this.tabPageSelect.UseVisualStyleBackColor = true;
            // 
            // dataGridViewS
            // 
            this.dataGridViewS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewS.Location = new System.Drawing.Point(3, 104);
            this.dataGridViewS.Name = "dataGridViewS";
            this.dataGridViewS.Size = new System.Drawing.Size(786, 317);
            this.dataGridViewS.TabIndex = 1;
            // 
            // groupBoxSelectChooseCourse
            // 
            this.groupBoxSelectChooseCourse.Controls.Add(this.buttonFindCourses);
            this.groupBoxSelectChooseCourse.Controls.Add(this.textBoxPriceFrom);
            this.groupBoxSelectChooseCourse.Controls.Add(this.groupBoxDetail);
            this.groupBoxSelectChooseCourse.Controls.Add(this.labelPriceTo);
            this.groupBoxSelectChooseCourse.Controls.Add(this.textBoxCourseName);
            this.groupBoxSelectChooseCourse.Controls.Add(this.textBoxPriceTo);
            this.groupBoxSelectChooseCourse.Controls.Add(this.labeCourseName);
            this.groupBoxSelectChooseCourse.Controls.Add(this.labelPriceFrom);
            this.groupBoxSelectChooseCourse.Controls.Add(this.checkBoxSort);
            this.groupBoxSelectChooseCourse.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxSelectChooseCourse.Location = new System.Drawing.Point(3, 3);
            this.groupBoxSelectChooseCourse.Name = "groupBoxSelectChooseCourse";
            this.groupBoxSelectChooseCourse.Size = new System.Drawing.Size(786, 101);
            this.groupBoxSelectChooseCourse.TabIndex = 0;
            this.groupBoxSelectChooseCourse.TabStop = false;
            this.groupBoxSelectChooseCourse.Text = "groupBoxChooseCourse";
            // 
            // buttonFindCourses
            // 
            this.buttonFindCourses.Location = new System.Drawing.Point(381, 71);
            this.buttonFindCourses.Name = "buttonFindCourses";
            this.buttonFindCourses.Size = new System.Drawing.Size(75, 23);
            this.buttonFindCourses.TabIndex = 11;
            this.buttonFindCourses.Text = "Find";
            this.buttonFindCourses.UseVisualStyleBackColor = true;
            this.buttonFindCourses.Click += new System.EventHandler(this.buttonFindCourses_Click);
            // 
            // textBoxPriceFrom
            // 
            this.textBoxPriceFrom.Location = new System.Drawing.Point(9, 73);
            this.textBoxPriceFrom.Name = "textBoxPriceFrom";
            this.textBoxPriceFrom.Size = new System.Drawing.Size(157, 20);
            this.textBoxPriceFrom.TabIndex = 10;
            this.textBoxPriceFrom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPriceTo_KeyPress);
            // 
            // groupBoxDetail
            // 
            this.groupBoxDetail.Controls.Add(this.radioButton3);
            this.groupBoxDetail.Controls.Add(this.radioButtonMarketing);
            this.groupBoxDetail.Controls.Add(this.radioButtonDesign);
            this.groupBoxDetail.Controls.Add(this.radioButtonIT);
            this.groupBoxDetail.Location = new System.Drawing.Point(312, 12);
            this.groupBoxDetail.Name = "groupBoxDetail";
            this.groupBoxDetail.Size = new System.Drawing.Size(420, 51);
            this.groupBoxDetail.TabIndex = 2;
            this.groupBoxDetail.TabStop = false;
            this.groupBoxDetail.Text = "Detail information about the coure ";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(345, 19);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(67, 17);
            this.radioButton3.TabIndex = 7;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "No detail";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButtonMarketing
            // 
            this.radioButtonMarketing.AutoSize = true;
            this.radioButtonMarketing.Location = new System.Drawing.Point(259, 19);
            this.radioButtonMarketing.Name = "radioButtonMarketing";
            this.radioButtonMarketing.Size = new System.Drawing.Size(80, 17);
            this.radioButtonMarketing.TabIndex = 6;
            this.radioButtonMarketing.Text = "Маркетинг";
            this.radioButtonMarketing.UseVisualStyleBackColor = true;
            // 
            // radioButtonDesign
            // 
            this.radioButtonDesign.AutoSize = true;
            this.radioButtonDesign.Location = new System.Drawing.Point(189, 19);
            this.radioButtonDesign.Name = "radioButtonDesign";
            this.radioButtonDesign.Size = new System.Drawing.Size(64, 17);
            this.radioButtonDesign.TabIndex = 5;
            this.radioButtonDesign.Text = "Дизайн";
            this.radioButtonDesign.UseVisualStyleBackColor = true;
            // 
            // radioButtonIT
            // 
            this.radioButtonIT.AutoSize = true;
            this.radioButtonIT.Location = new System.Drawing.Point(6, 19);
            this.radioButtonIT.Name = "radioButtonIT";
            this.radioButtonIT.Size = new System.Drawing.Size(177, 17);
            this.radioButtonIT.TabIndex = 0;
            this.radioButtonIT.Text = "Информационные технологии";
            this.radioButtonIT.UseVisualStyleBackColor = true;
            // 
            // labelPriceTo
            // 
            this.labelPriceTo.AutoSize = true;
            this.labelPriceTo.Location = new System.Drawing.Point(184, 57);
            this.labelPriceTo.Name = "labelPriceTo";
            this.labelPriceTo.Size = new System.Drawing.Size(43, 13);
            this.labelPriceTo.TabIndex = 9;
            this.labelPriceTo.Text = "Price to";
            // 
            // textBoxCourseName
            // 
            this.textBoxCourseName.Location = new System.Drawing.Point(90, 20);
            this.textBoxCourseName.Name = "textBoxCourseName";
            this.textBoxCourseName.Size = new System.Drawing.Size(202, 20);
            this.textBoxCourseName.TabIndex = 1;
            // 
            // textBoxPriceTo
            // 
            this.textBoxPriceTo.Location = new System.Drawing.Point(176, 73);
            this.textBoxPriceTo.Name = "textBoxPriceTo";
            this.textBoxPriceTo.Size = new System.Drawing.Size(157, 20);
            this.textBoxPriceTo.TabIndex = 2;
            this.textBoxPriceTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPriceFrom_KeyPress);
            // 
            // labeCourseName
            // 
            this.labeCourseName.AutoSize = true;
            this.labeCourseName.Location = new System.Drawing.Point(6, 23);
            this.labeCourseName.Name = "labeCourseName";
            this.labeCourseName.Size = new System.Drawing.Size(69, 13);
            this.labeCourseName.TabIndex = 0;
            this.labeCourseName.Text = "Course name";
            // 
            // labelPriceFrom
            // 
            this.labelPriceFrom.AutoSize = true;
            this.labelPriceFrom.Location = new System.Drawing.Point(15, 57);
            this.labelPriceFrom.Name = "labelPriceFrom";
            this.labelPriceFrom.Size = new System.Drawing.Size(54, 13);
            this.labelPriceFrom.TabIndex = 8;
            this.labelPriceFrom.Text = "Price from";
            // 
            // checkBoxSort
            // 
            this.checkBoxSort.AutoSize = true;
            this.checkBoxSort.Location = new System.Drawing.Point(489, 75);
            this.checkBoxSort.Name = "checkBoxSort";
            this.checkBoxSort.Size = new System.Drawing.Size(85, 17);
            this.checkBoxSort.TabIndex = 4;
            this.checkBoxSort.Text = "Sort by price";
            this.checkBoxSort.UseVisualStyleBackColor = true;
            // 
            // tabPageSubquery
            // 
            this.tabPageSubquery.Controls.Add(this.dataGridView1);
            this.tabPageSubquery.Controls.Add(this.groupBoxSubquery);
            this.tabPageSubquery.Location = new System.Drawing.Point(4, 22);
            this.tabPageSubquery.Name = "tabPageSubquery";
            this.tabPageSubquery.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSubquery.Size = new System.Drawing.Size(792, 424);
            this.tabPageSubquery.TabIndex = 2;
            this.tabPageSubquery.Text = "Subquery";
            this.tabPageSubquery.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 56);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(786, 365);
            this.dataGridView1.TabIndex = 1;
            // 
            // groupBoxSubquery
            // 
            this.groupBoxSubquery.Controls.Add(this.buttonExecute);
            this.groupBoxSubquery.Controls.Add(this.textBoxNumberSub);
            this.groupBoxSubquery.Controls.Add(this.labelNumber);
            this.groupBoxSubquery.Controls.Add(this.radioButtonNoCorrelated);
            this.groupBoxSubquery.Controls.Add(this.radioButtonCorrelated);
            this.groupBoxSubquery.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxSubquery.Location = new System.Drawing.Point(3, 3);
            this.groupBoxSubquery.Name = "groupBoxSubquery";
            this.groupBoxSubquery.Size = new System.Drawing.Size(786, 53);
            this.groupBoxSubquery.TabIndex = 0;
            this.groupBoxSubquery.TabStop = false;
            this.groupBoxSubquery.Text = "data subqueries";
            // 
            // buttonExecute
            // 
            this.buttonExecute.Location = new System.Drawing.Point(479, 16);
            this.buttonExecute.Name = "buttonExecute";
            this.buttonExecute.Size = new System.Drawing.Size(75, 23);
            this.buttonExecute.TabIndex = 4;
            this.buttonExecute.Text = "execute";
            this.buttonExecute.UseVisualStyleBackColor = true;
            this.buttonExecute.Click += new System.EventHandler(this.buttonExecute_Click);
            // 
            // textBoxNumberSub
            // 
            this.textBoxNumberSub.Location = new System.Drawing.Point(384, 18);
            this.textBoxNumberSub.Name = "textBoxNumberSub";
            this.textBoxNumberSub.Size = new System.Drawing.Size(58, 20);
            this.textBoxNumberSub.TabIndex = 3;
            this.textBoxNumberSub.TextChanged += new System.EventHandler(this.textBoxNumberSub_TextChanged);
            this.textBoxNumberSub.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNumberSub_KeyPress);
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Location = new System.Drawing.Point(214, 23);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(117, 13);
            this.labelNumber.TabIndex = 2;
            this.labelNumber.Text = "subscribe to the course";
            // 
            // radioButtonNoCorrelated
            // 
            this.radioButtonNoCorrelated.AutoSize = true;
            this.radioButtonNoCorrelated.Location = new System.Drawing.Point(97, 19);
            this.radioButtonNoCorrelated.Name = "radioButtonNoCorrelated";
            this.radioButtonNoCorrelated.Size = new System.Drawing.Size(87, 17);
            this.radioButtonNoCorrelated.TabIndex = 1;
            this.radioButtonNoCorrelated.Text = "NoCorrelated";
            this.radioButtonNoCorrelated.UseVisualStyleBackColor = true;
            // 
            // radioButtonCorrelated
            // 
            this.radioButtonCorrelated.AutoSize = true;
            this.radioButtonCorrelated.Checked = true;
            this.radioButtonCorrelated.Location = new System.Drawing.Point(6, 19);
            this.radioButtonCorrelated.Name = "radioButtonCorrelated";
            this.radioButtonCorrelated.Size = new System.Drawing.Size(73, 17);
            this.radioButtonCorrelated.TabIndex = 0;
            this.radioButtonCorrelated.TabStop = true;
            this.radioButtonCorrelated.Text = "Correlated";
            this.radioButtonCorrelated.UseVisualStyleBackColor = true;
            this.radioButtonCorrelated.CheckedChanged += new System.EventHandler(this.radioButtonCorrelated_CheckedChanged);
            // 
            // tabPageDML
            // 
            this.tabPageDML.Controls.Add(this.dataGridView2);
            this.tabPageDML.Controls.Add(this.buttonShow);
            this.tabPageDML.Controls.Add(this.panel);
            this.tabPageDML.Controls.Add(this.groupBoxoperat);
            this.tabPageDML.Location = new System.Drawing.Point(4, 22);
            this.tabPageDML.Name = "tabPageDML";
            this.tabPageDML.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDML.Size = new System.Drawing.Size(792, 424);
            this.tabPageDML.TabIndex = 3;
            this.tabPageDML.Text = "Requests for data changes";
            this.tabPageDML.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(0, 163);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(789, 258);
            this.dataGridView2.TabIndex = 7;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(355, 134);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(75, 23);
            this.buttonShow.TabIndex = 6;
            this.buttonShow.Text = "Show";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.buttonShow_Click);
            // 
            // panel
            // 
            this.panel.Controls.Add(this.label5);
            this.panel.Controls.Add(this.label4);
            this.panel.Controls.Add(this.label3);
            this.panel.Controls.Add(this.comboBox1);
            this.panel.Controls.Add(this.textBox2);
            this.panel.Controls.Add(this.textBox1);
            this.panel.Controls.Add(this.textBoxNam);
            this.panel.Controls.Add(this.labC);
            this.panel.Location = new System.Drawing.Point(3, 84);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(786, 44);
            this.panel.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(613, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Gender";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(395, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Middle name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(193, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Surname";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "мужской",
            "женский"});
            this.comboBox1.Location = new System.Drawing.Point(656, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(130, 21);
            this.comboBox1.TabIndex = 9;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(468, 7);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(130, 20);
            this.textBox2.TabIndex = 8;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(248, 7);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(130, 20);
            this.textBox1.TabIndex = 7;
            // 
            // textBoxNam
            // 
            this.textBoxNam.Location = new System.Drawing.Point(46, 7);
            this.textBoxNam.Name = "textBoxNam";
            this.textBoxNam.Size = new System.Drawing.Size(130, 20);
            this.textBoxNam.TabIndex = 6;
            // 
            // labC
            // 
            this.labC.AutoSize = true;
            this.labC.Location = new System.Drawing.Point(5, 10);
            this.labC.Name = "labC";
            this.labC.Size = new System.Drawing.Size(35, 13);
            this.labC.TabIndex = 6;
            this.labC.Text = "Name";
            // 
            // groupBoxoperat
            // 
            this.groupBoxoperat.Controls.Add(this.ExecuteButt);
            this.groupBoxoperat.Controls.Add(this.textBoxNumber);
            this.groupBoxoperat.Controls.Add(this.label6);
            this.groupBoxoperat.Controls.Add(this.textBoxSeries);
            this.groupBoxoperat.Controls.Add(this.labelA);
            this.groupBoxoperat.Controls.Add(this.radioButtonDel);
            this.groupBoxoperat.Controls.Add(this.radioButtonUpt);
            this.groupBoxoperat.Controls.Add(this.radioButtonInsert);
            this.groupBoxoperat.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxoperat.Location = new System.Drawing.Point(3, 3);
            this.groupBoxoperat.Name = "groupBoxoperat";
            this.groupBoxoperat.Size = new System.Drawing.Size(786, 75);
            this.groupBoxoperat.TabIndex = 0;
            this.groupBoxoperat.TabStop = false;
            this.groupBoxoperat.Text = "Operators";
            // 
            // ExecuteButt
            // 
            this.ExecuteButt.Location = new System.Drawing.Point(352, 39);
            this.ExecuteButt.Name = "ExecuteButt";
            this.ExecuteButt.Size = new System.Drawing.Size(75, 23);
            this.ExecuteButt.TabIndex = 13;
            this.ExecuteButt.Text = "Execute";
            this.ExecuteButt.UseVisualStyleBackColor = true;
            this.ExecuteButt.Click += new System.EventHandler(this.ExecuteButt_Click);
            // 
            // textBoxNumber
            // 
            this.textBoxNumber.Location = new System.Drawing.Point(231, 42);
            this.textBoxNumber.Name = "textBoxNumber";
            this.textBoxNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxNumber.TabIndex = 12;
            this.textBoxNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNumber_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(170, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Number";
            // 
            // textBoxSeries
            // 
            this.textBoxSeries.Location = new System.Drawing.Point(55, 42);
            this.textBoxSeries.Name = "textBoxSeries";
            this.textBoxSeries.Size = new System.Drawing.Size(100, 20);
            this.textBoxSeries.TabIndex = 10;
            this.textBoxSeries.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSeries_KeyPress);
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Location = new System.Drawing.Point(3, 45);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(36, 13);
            this.labelA.TabIndex = 9;
            this.labelA.Text = "Series";
            // 
            // radioButtonDel
            // 
            this.radioButtonDel.AutoSize = true;
            this.radioButtonDel.Location = new System.Drawing.Point(154, 19);
            this.radioButtonDel.Name = "radioButtonDel";
            this.radioButtonDel.Size = new System.Drawing.Size(56, 17);
            this.radioButtonDel.TabIndex = 2;
            this.radioButtonDel.TabStop = true;
            this.radioButtonDel.Text = "Delete";
            this.radioButtonDel.UseVisualStyleBackColor = true;
            this.radioButtonDel.CheckedChanged += new System.EventHandler(this.radioButtonDel_CheckedChanged);
            // 
            // radioButtonUpt
            // 
            this.radioButtonUpt.AutoSize = true;
            this.radioButtonUpt.Location = new System.Drawing.Point(76, 19);
            this.radioButtonUpt.Name = "radioButtonUpt";
            this.radioButtonUpt.Size = new System.Drawing.Size(60, 17);
            this.radioButtonUpt.TabIndex = 1;
            this.radioButtonUpt.TabStop = true;
            this.radioButtonUpt.Text = "Update";
            this.radioButtonUpt.UseVisualStyleBackColor = true;
            // 
            // radioButtonInsert
            // 
            this.radioButtonInsert.AutoSize = true;
            this.radioButtonInsert.Location = new System.Drawing.Point(6, 19);
            this.radioButtonInsert.Name = "radioButtonInsert";
            this.radioButtonInsert.Size = new System.Drawing.Size(51, 17);
            this.radioButtonInsert.TabIndex = 0;
            this.radioButtonInsert.TabStop = true;
            this.radioButtonInsert.Text = "Insert";
            this.radioButtonInsert.UseVisualStyleBackColor = true;
            // 
            // SQL_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControlSQL);
            this.Name = "SQL_Form";
            this.Text = "SQL query";
            this.tabControlSQL.ResumeLayout(false);
            this.tabPageExample.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSelect)).EndInit();
            this.groupBoxSelect.ResumeLayout(false);
            this.groupBoxSelect.PerformLayout();
            this.tabPageSelect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewS)).EndInit();
            this.groupBoxSelectChooseCourse.ResumeLayout(false);
            this.groupBoxSelectChooseCourse.PerformLayout();
            this.groupBoxDetail.ResumeLayout(false);
            this.groupBoxDetail.PerformLayout();
            this.tabPageSubquery.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBoxSubquery.ResumeLayout(false);
            this.groupBoxSubquery.PerformLayout();
            this.tabPageDML.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.groupBoxoperat.ResumeLayout(false);
            this.groupBoxoperat.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlSQL;
        private System.Windows.Forms.TabPage tabPageExample;
        private System.Windows.Forms.DataGridView dataGridViewSelect;
        private System.Windows.Forms.GroupBox groupBoxSelect;
        private System.Windows.Forms.RadioButton radioButtonStudentName;
        private System.Windows.Forms.RadioButton radioButtonStudents;
        private System.Windows.Forms.RadioButton radioButtonCourses;
        private System.Windows.Forms.TabPage tabPageSelect;
        private System.Windows.Forms.GroupBox groupBoxSelectChooseCourse;
        private System.Windows.Forms.GroupBox groupBoxDetail;
        private System.Windows.Forms.RadioButton radioButtonDesign;
        private System.Windows.Forms.CheckBox checkBoxSort;
        private System.Windows.Forms.TextBox textBoxPriceTo;
        private System.Windows.Forms.RadioButton radioButtonIT;
        private System.Windows.Forms.TextBox textBoxCourseName;
        private System.Windows.Forms.Label labeCourseName;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButtonMarketing;
        private System.Windows.Forms.TextBox textBoxPriceFrom;
        private System.Windows.Forms.Label labelPriceTo;
        private System.Windows.Forms.Label labelPriceFrom;
        private System.Windows.Forms.DataGridView dataGridViewS;
        private System.Windows.Forms.Button buttonFindCourses;
        private System.Windows.Forms.TabPage tabPageSubquery;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBoxSubquery;
        private System.Windows.Forms.Button buttonExecute;
        private System.Windows.Forms.TextBox textBoxNumberSub;
        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.RadioButton radioButtonNoCorrelated;
        private System.Windows.Forms.RadioButton radioButtonCorrelated;
        private System.Windows.Forms.TabPage tabPageDML;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBoxNam;
        private System.Windows.Forms.Label labC;
        private System.Windows.Forms.GroupBox groupBoxoperat;
        private System.Windows.Forms.RadioButton radioButtonDel;
        private System.Windows.Forms.RadioButton radioButtonUpt;
        private System.Windows.Forms.RadioButton radioButtonInsert;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Button ExecuteButt;
        private System.Windows.Forms.TextBox textBoxNumber;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxSeries;
        private System.Windows.Forms.Label labelA;
    }
}