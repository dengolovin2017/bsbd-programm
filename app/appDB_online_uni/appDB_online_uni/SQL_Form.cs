﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace appDB_online_uni
{
    public partial class SQL_Form : Form
    {
        private static SQL_Form f;

        private SQL_Form()
        {
            InitializeComponent();
        }

        public static SQL_Form GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new SQL_Form();
            }

            return f;
        }

        private void radioButtonCourses_CheckedChanged(object sender, EventArgs e)
        {
            dataGridViewSelect.DataSource = FillDataGridView("SELECT * FROM Course");
        }

        private DataTable FillDataGridView(string sqlQuery)
        {
            SqlConnection connection = new SqlConnection(Properties.Settings.Default.Online_UniversityConnectionString);       
            SqlCommand command = connection.CreateCommand();
            command.CommandText = sqlQuery;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable table = new DataTable();
            adapter.Fill(table);

            return table;          
        }

        private void radioButtonStudents_CheckedChanged(object sender, EventArgs e)
        {
            dataGridViewSelect.DataSource = FillDataGridView(@"SELECT id AS Number, passport_series + ' - ' " +
                "+ passport_number AS Passport, registration_date AS Date FROM Student");
        }

        private void radioButtonTeachers_CheckedChanged(object sender, EventArgs e)
        {
            dataGridViewSelect.DataSource = FillDataGridView(@"SELECT Passport.name + ' ' + Passport.surname + ' ' +" +
                " Passport.middle_name AS Full_name, Student.registration_date AS Registration_date FROM Passport, Student " +
                "WHERE Passport.series = Student.passport_series AND Passport.number = Student.passport_number");
        }
        
        private void buttonFindCourses_Click(object sender, EventArgs e)
        {
            string command, where = "", group;
            if (!String.IsNullOrWhiteSpace(textBoxCourseName.Text)) 
            {       
                where = "name LIKE '%" + textBoxCourseName.Text + "%' AND ";
            }

            if (!String.IsNullOrWhiteSpace(textBoxPriceFrom.Text))
            {
                where += "price >= " + textBoxPriceFrom.Text + " AND ";
            }

            if (!String.IsNullOrWhiteSpace(textBoxPriceTo.Text))
            {
                where += "price <= " + textBoxPriceTo.Text + " AND ";
            }

            if (!radioButton3.Checked)
            {
                foreach (RadioButton rb in groupBoxDetail.Controls)
                {
                    if (rb.Checked)
                    {
                        where += "type = '" + rb.Text + "' AND ";
                    }
                }
            }

            if (where.Length - 5 >= 0)
            {
                where = where.Substring(0, where.Length - 5);
            }

            if (where.Equals(""))
            {
                command = @"SELECT * FROM Course";
            } else
            {
                command = @"SELECT * FROM Course WHERE " + where;
            }
            

            if (checkBoxSort.Checked)
            {
                command += " ORDER BY price desc";
            }

            SqlConnection connection = new SqlConnection(Properties.Settings.Default.Online_UniversityConnectionString);

            SqlCommand sqlCommand = connection.CreateCommand();

            sqlCommand.CommandText = command;

            SqlDataAdapter adapter = new SqlDataAdapter();

            adapter.SelectCommand = sqlCommand;

            DataTable table = new DataTable();

            adapter.Fill(table);

            dataGridViewS.DataSource = table;

            if (table.Rows.Count == 0)
            {
                MessageBox.Show("Нет таких курсов!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void textBoxPriceTo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void textBoxPriceFrom_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void textBoxNumberSub_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void textBoxSeries_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void textBoxNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void buttonExecute_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(textBoxNumberSub.Text))
            {
                MessageBox.Show("Введите номер курса!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string command = "";

            if (radioButtonCorrelated.Checked)
            {
                command = @"SELECT id, registration_date FROM Student WHERE " + textBoxNumberSub.Text + " IN (SELECT student_id FROM Student_Course WHERE Student_id = Student.id)";
            } else
            {
                command = @"SELECT id, registration_date FROM Student WHERE id IN (SELECT student_id FROM Student_Course WHERE Student_id = " + textBoxNumberSub.Text + ")";
            }

            SqlConnection connection = new SqlConnection(Properties.Settings.Default.Online_UniversityConnectionString);

            SqlCommand sqlCommand = connection.CreateCommand();

            sqlCommand.CommandText = command;

            SqlDataAdapter adapter = new SqlDataAdapter();

            adapter.SelectCommand = sqlCommand;

            DataTable table = new DataTable();

            adapter.Fill(table);

            dataGridView1.DataSource = table;

            if (table.Rows.Count == 0)
            {
                MessageBox.Show("Нет таких курсов!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        void InsertData()
        {
            if (String.IsNullOrWhiteSpace(textBoxSeries.Text) || String.IsNullOrWhiteSpace(textBox2.Text) ||
                String.IsNullOrWhiteSpace(textBoxNumber.Text) || String.IsNullOrWhiteSpace(textBox1.Text) ||
                String.IsNullOrWhiteSpace(textBoxNam.Text) || String.IsNullOrWhiteSpace(comboBox1.Text))
            {
                MessageBox.Show("Заполните все поля!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string command = @"INSERT INTO Passport (series, number, name, surname, middle_name, gender)
                VALUES (@series, @number, @name, @surname, @middle, @gender)";

            SqlConnection connection = new SqlConnection(Properties.Settings.Default.Online_UniversityConnectionString);

            connection.Open();

            SqlCommand sqlCommand = connection.CreateCommand();

            sqlCommand.CommandText = command;

            sqlCommand.Parameters.AddWithValue("@series", textBoxSeries.Text);
            sqlCommand.Parameters.AddWithValue("@number", textBoxNumber.Text);
            sqlCommand.Parameters.AddWithValue("@name", textBoxNam.Text);
            sqlCommand.Parameters.AddWithValue("@surname", textBox1.Text);
            sqlCommand.Parameters.AddWithValue("@middle", textBox2.Text);
            sqlCommand.Parameters.AddWithValue("@gender", comboBox1.Text);

            try
            {
                sqlCommand.ExecuteNonQuery();
            } 
            catch(Exception ex)
            {
                MessageBox.Show("Ошибка выполнения запроса\n" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            connection.Close();
            buttonShow_Click(this, EventArgs.Empty);
        }

        void UpdateData()
        {
            if (String.IsNullOrWhiteSpace(textBoxSeries.Text) || String.IsNullOrWhiteSpace(textBoxNumber.Text))
            {
                MessageBox.Show("Заполните все поля!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string command = @"UPDATE Passport SET ";
            int payload = 0;

            if (!String.IsNullOrWhiteSpace(textBoxNam.Text))
            {
                command += "name='" + textBoxNam.Text + "', ";
                payload++;
            }

            if (!String.IsNullOrWhiteSpace(textBox1.Text))
            {
                command += "surname='" + textBox1.Text + "', ";
                payload++;
            }

            if (!String.IsNullOrWhiteSpace(textBox2.Text))
            {
                command += "middle_name='" + textBox2.Text + "', ";
                payload++;
            }

            if (!String.IsNullOrWhiteSpace(comboBox1.Text))
            {
                command += "gender='" + comboBox1.Text + "', ";
                payload++;
            }

            if (payload > 0)
            {
                command = command.Substring(0, command.Length - 2);
                command += " FROM Passport WHERE series=" + textBoxSeries.Text + "AND number=" + textBoxNumber.Text;
            } else
            {
                return;
            }

            SqlConnection connection = new SqlConnection(Properties.Settings.Default.Online_UniversityConnectionString);

            connection.Open();

            SqlCommand sqlCommand = connection.CreateCommand();

            sqlCommand.CommandText = command;

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка выполнения запроса\n" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            connection.Close();
            buttonShow_Click(this, EventArgs.Empty);
        }

        private void DeleteData()
        {
            if (String.IsNullOrWhiteSpace(textBoxSeries.Text) || String.IsNullOrWhiteSpace(textBoxNumber.Text))
            {
                MessageBox.Show("Заполните все поля!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string command = @"DELETE FROM Passport WHERE series=@series AND number=@number";

            SqlConnection connection = new SqlConnection(Properties.Settings.Default.Online_UniversityConnectionString);

            connection.Open();

            SqlCommand sqlCommand = connection.CreateCommand();

            sqlCommand.CommandText = command;

            sqlCommand.Parameters.AddWithValue("@series", textBoxSeries.Text);
            sqlCommand.Parameters.AddWithValue("@number", textBoxNumber.Text);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка выполнения запроса\n" + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            connection.Close();
            buttonShow_Click(this, EventArgs.Empty);

        }


        private void buttonShow_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = 
                ("SELECT * FROM Passport");
        }

        private void radioButtonDel_CheckedChanged(object sender, EventArgs e)
        {
            panel.Visible = !radioButtonDel.Checked;
        }

        private void ExecuteButt_Click(object sender, EventArgs e)
        {
            if (radioButtonInsert.Checked) InsertData();
            else if (radioButtonUpt.Checked) UpdateData();
            else if (radioButtonDel.Checked) DeleteData();
            else
            {
                MessageBox.Show("Выберите запрос", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBoxSeries.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[0].Value);
            textBoxNumber.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[1].Value);
            textBoxNam.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[2].Value);
            textBox1.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[3].Value);
            textBox2.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[4].Value);
            comboBox1.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[5].Value);
        }

        private void radioButtonCorrelated_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBoxNumberSub_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
