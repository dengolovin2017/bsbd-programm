﻿namespace appDB_online_uni
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CoursesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TeachersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PassportDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.JournalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SubscriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sQLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sqlQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.online_UniversityDataSet = new appDB_online_uni.Online_UniversityDataSet();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.userTableAdapter = new appDB_online_uni.Online_UniversityDataSetTableAdapters.UsersTableAdapter();
            this.tableAdapterManager = new appDB_online_uni.Online_UniversityDataSetTableAdapters.TableAdapterManager();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.online_UniversityDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.справочникиToolStripMenuItem,
            this.sQLToolStripMenuItem,
            this.adminToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(48, 20);
            this.toolStripMenuItem1.Text = "Файл";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.exit;
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.CloseToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.about_programm;
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F10)));
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.AboutProgrammToolStripMenuItem_Click);
            // 
            // справочникиToolStripMenuItem
            // 
            this.справочникиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CoursesToolStripMenuItem,
            this.StudentsToolStripMenuItem,
            this.TeachersToolStripMenuItem,
            this.PassportDataToolStripMenuItem,
            this.DZToolStripMenuItem,
            this.JournalToolStripMenuItem,
            this.SubscriptionToolStripMenuItem});
            this.справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            this.справочникиToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.справочникиToolStripMenuItem.Text = "Справочники";
            // 
            // CoursesToolStripMenuItem
            // 
            this.CoursesToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.Course;
            this.CoursesToolStripMenuItem.Name = "CoursesToolStripMenuItem";
            this.CoursesToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.CoursesToolStripMenuItem.Text = "Курсы";
            this.CoursesToolStripMenuItem.Click += new System.EventHandler(this.CoursesToolStripMenuItem_Click);
            // 
            // StudentsToolStripMenuItem
            // 
            this.StudentsToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.Student;
            this.StudentsToolStripMenuItem.Name = "StudentsToolStripMenuItem";
            this.StudentsToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.StudentsToolStripMenuItem.Text = "Студенты";
            this.StudentsToolStripMenuItem.Click += new System.EventHandler(this.StudentsToolStripMenuItem_Click);
            // 
            // TeachersToolStripMenuItem
            // 
            this.TeachersToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.Teach;
            this.TeachersToolStripMenuItem.Name = "TeachersToolStripMenuItem";
            this.TeachersToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.TeachersToolStripMenuItem.Text = "Учителя";
            this.TeachersToolStripMenuItem.Click += new System.EventHandler(this.TeachersToolStripMenuItem_Click);
            // 
            // PassportDataToolStripMenuItem
            // 
            this.PassportDataToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.pass;
            this.PassportDataToolStripMenuItem.Name = "PassportDataToolStripMenuItem";
            this.PassportDataToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.PassportDataToolStripMenuItem.Text = "Паспортные данные";
            this.PassportDataToolStripMenuItem.Click += new System.EventHandler(this.PassportDataToolStripMenuItem_Click);
            // 
            // DZToolStripMenuItem
            // 
            this.DZToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.Course;
            this.DZToolStripMenuItem.Name = "DZToolStripMenuItem";
            this.DZToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.DZToolStripMenuItem.Text = "ДЗ";
            this.DZToolStripMenuItem.Click += new System.EventHandler(this.DZToolStripMenuItem_Click);
            // 
            // JournalToolStripMenuItem
            // 
            this.JournalToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.about_programm;
            this.JournalToolStripMenuItem.Name = "JournalToolStripMenuItem";
            this.JournalToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.JournalToolStripMenuItem.Text = "Журнал";
            this.JournalToolStripMenuItem.Click += new System.EventHandler(this.JournalToolStripMenuItem_Click);
            // 
            // SubscriptionToolStripMenuItem
            // 
            this.SubscriptionToolStripMenuItem.Image = global::appDB_online_uni.Properties.Resources.exit;
            this.SubscriptionToolStripMenuItem.Name = "SubscriptionToolStripMenuItem";
            this.SubscriptionToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.SubscriptionToolStripMenuItem.Text = "Подписки";
            this.SubscriptionToolStripMenuItem.Click += new System.EventHandler(this.SubscriptionToolStripMenuItem_Click);
            // 
            // sQLToolStripMenuItem
            // 
            this.sQLToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sqlQueryToolStripMenuItem});
            this.sQLToolStripMenuItem.Name = "sQLToolStripMenuItem";
            this.sQLToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.sQLToolStripMenuItem.Text = "SQL";
            // 
            // sqlQueryToolStripMenuItem
            // 
            this.sqlQueryToolStripMenuItem.Name = "sqlQueryToolStripMenuItem";
            this.sqlQueryToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.sqlQueryToolStripMenuItem.Text = "sql query";
            this.sqlQueryToolStripMenuItem.Click += new System.EventHandler(this.sqlQueryToolStripMenuItem_Click);
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(134, 20);
            this.adminToolStripMenuItem.Text = "Администрирование";
            this.adminToolStripMenuItem.Click += new System.EventHandler(this.adminToolStripMenuItem_Click);
            // 
            // online_UniversityDataSet
            // 
            this.online_UniversityDataSet.DataSetName = "Online_UniversityDataSet";
            this.online_UniversityDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataMember = "User";
            this.userBindingSource.DataSource = this.online_UniversityDataSet;
            // 
            // userTableAdapter
            // 
            this.userTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CourseTableAdapter = null;
            this.tableAdapterManager.HomeworkTableAdapter = null;
            this.tableAdapterManager.JournalTableAdapter = null;
            this.tableAdapterManager.PassportTableAdapter = null;
            this.tableAdapterManager.Student_CourseTableAdapter = null;
            this.tableAdapterManager.StudentTableAdapter = null;
            this.tableAdapterManager.TeacherTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = appDB_online_uni.Online_UniversityDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UsersTableAdapter = this.userTableAdapter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(496, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ваша роль: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(566, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.online_UniversityDataSet)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справочникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CoursesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TeachersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PassportDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem JournalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SubscriptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sQLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sqlQueryToolStripMenuItem;
        private Online_UniversityDataSet online_UniversityDataSet;
        private System.Windows.Forms.BindingSource userBindingSource;
        private Online_UniversityDataSetTableAdapters.UsersTableAdapter userTableAdapter;
        private Online_UniversityDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

