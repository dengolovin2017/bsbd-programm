﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class PassportForm : Form
    {

        private static PassportForm f;

        private PassportForm()
        {
            InitializeComponent();
        }

        public static PassportForm GetInstance()
        {
            if (f == null || f.IsDisposed)
            {
                f = new PassportForm();
            }

            return f;
        }

        private void passportBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.passportBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);

        }

        private void passportBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.passportBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);

        }

        private void PassportForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Teacher". При необходимости она может быть перемещена или удалена.
            this.teacherTableAdapter.Fill(this.online_UniversityDataSet.Teacher);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Student". При необходимости она может быть перемещена или удалена.
            this.studentTableAdapter.Fill(this.online_UniversityDataSet.Student);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "online_UniversityDataSet.Passport". При необходимости она может быть перемещена или удалена.
            this.passportTableAdapter.Fill(this.online_UniversityDataSet.Passport);

        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            try
            { 
            this.Validate();
            this.studentBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            } catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}

        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.teacherBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.online_UniversityDataSet);
            } catch (Exception er)
            {
                MessageBox.Show(er.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ButtonFind_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxFind_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
