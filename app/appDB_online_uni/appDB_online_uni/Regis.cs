﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appDB_online_uni
{
    public partial class Regis : Form
    {

        private string connStr = @"Data Source=DESKTOP-BU6BBHD\SQLEXPRESS;Initial Catalog=Online_University;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public Regis()
        {
            InitializeComponent();
            passText.UseSystemPasswordChar = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string lt = loginText.Text.ToLower().Trim();

            if (lt.Contains("select") || lt.Contains("insert") || lt.Contains("delete") || lt.Contains("update") ||
                lt.Contains("'") || lt.Contains("/") || lt.Contains("where") || lt.Contains("="))
            {
                MessageBox.Show("Недопустивые символы!");
                return;
            }

            using (SqlConnection conn = new SqlConnection(connStr))
            {
                conn.Open();
                MD5 md5 = new MD5CryptoServiceProvider();
                string md5hash = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes((passText.Text))))
                .Replace("-", String.Empty).ToLower();
                string sql = "SELECT id, password, role FROM Users WHERE login = '" + loginText.Text + "'";

                using (SqlCommand cmd = new SqlCommand(sql, conn))
                using (SqlDataReader reader = cmd.ExecuteReader())
                {

                    if (reader.HasRows)
                    {

                        reader.Read();
                        if (md5hash == reader.GetString(1).Trim())
                        {
                            MainForm f = new MainForm(reader.GetString(2).Trim());
                            f.Show();
                            this.Hide();
                        }

                    }
                    reader.Close();
                    conn.Close();
                }
            }
        }

        private void signUp_Click(object sender, EventArgs e)
        {

            if (!loginText.Text.Equals("") && !passText.Text.Equals("") && passText.Text.Length > 7)
            {
                string lt = loginText.Text.ToLower().Trim();

                if (lt.Contains("select") || lt.Contains("insert") || lt.Contains("delete") || lt.Contains("update") ||
                    lt.Contains("'") || lt.Contains("/") || lt.Contains("where") || lt.Contains("="))
                {
                    MessageBox.Show("Недопустивые символы!");
                    return;
                }


                using (SqlConnection conn = new SqlConnection(connStr))
                {

                    MD5 md5 = new MD5CryptoServiceProvider();

                    string md5hash = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes((passText.Text))))
                    .Replace("-", String.Empty).ToLower();

                    SqlCommand sqlSel = new SqlCommand("SELECT id FROM Users WHERE login = '" + loginText.Text + "'", conn);
                    SqlCommand sqlIns = new SqlCommand("INSERT INTO Users (login, password, role) VALUES ('" +
                        loginText.Text + "','" + md5hash + "','simple');", conn);

                    conn.Open();
                    SqlDataReader reader = sqlSel.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        reader.Close();
                        sqlIns.ExecuteNonQuery();
                    }
                    reader.Close();
                    conn.Close();
                }
            }
            else
            {
                MessageBox.Show("Неверно задан логин или пароль!");      
            }
        }
    }
}

